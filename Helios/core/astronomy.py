"""Module for implementing geometry to create astronomy data."""

from math import cos, radians, sin

from Helios.core.geometry import Polygon


class Planet():
    """A planet orbiting arount the 40 Eridani A star.

    This class requires 2 mandatory arguments:

    - The distance the planet is from the star
    - The speed it orbits around
    """

    def __init__(self, distance, speed, name=None):

        self.name = name
        self.radius = distance
        self.speed = speed

        if speed < 0:
            self.sign = 1
        elif speed > 0:
            self.sign = -1
        else:
            self.sign = 0

        self.time_unit = {
            "days": 1,
            "hours": 24,
            "minutes": 1440,
        }

    def coordinates(self, time_unit, time):
        """Retutn the coordinates of the planet for a given day."""

        if isinstance(time_unit, str):
            try:
                time_unit = self.time_unit[time_unit]
            except KeyError as exception:
                del exception
                raise ValueError(
                    "{} is not a valid time time_unit".format(time_unit)
                )

        angle = time * (self.speed / time_unit)
        while abs(angle) > 359:
            angle += 360 * self.sign

        y_axis = sin(radians(angle)) * self.radius
        x_axis = cos(radians(angle)) * self.radius
        return x_axis, y_axis


class SolarSystem():
    """A number of planets oribiting around a star."""

    def __init__(self, planets, animation_object=None):

        self.planets = planets
        self.animation = animation_object

    def make_polygon(self, time_unit, time):
        """Return the axis values for the polygon made by the planets."""

        x_values = []
        y_values = []

        for planet in self.planets:
            x_value, y_value = planet.coordinates(time_unit, time)
            x_values.append(x_value)
            y_values.append(y_value)

        return x_values, y_values

    def check_weather(self, time_unit, time):
        """Return the weather conditions.

        Weather conditions can be a string, with the expected weather's
        description, a float, representing the amount of rain expected for
        that day, or None, if there are no defined conditions for that day.
        """

        polygon = Polygon(*self.make_polygon(time_unit, time))
        pertenence = polygon.check_point_pertenence([0, 0])
        alignment = polygon.check_points_alignment()

        if not pertenence and alignment:
            return "optimal"
        elif pertenence and alignment:
            return "drought"
        elif pertenence and not alignment:
            return polygon.calculate_perimeter()
        else:
            return None

    def animate(self, time_unit="hours"):
        """Show an animation of the time passing"""

        self.animation.animate(time_unit, self)

    def draw_time(self, time, time_unit="hours"):
        """Sow the drawing of a given time's position."""

        self.animation.draw_time(time, time_unit, self)

    def animate_time(self, start_time, end_time, time_unit="hours"):
        """Animate the given time frame."""

        self.animation.animate_time(start_time, end_time, time_unit, self)
