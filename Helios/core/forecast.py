"""The module for actual forecstas in Helio."""

from Helios.core.astronomy import SolarSystem


class WeatherForecast():
    """An object capable to generate and emit a forecast for a given period."""

    def __init__(self, solar_system, emitter=None):

        self.solar_system = solar_system
        self.emitter = emitter

    def forecast_day(self, day):
        """Return a dict with the forecast for de given day.

        The returned dict will have at least the 'día' and 'clima' keys,
        representing the day of the forecast and the string that represents
        the weather.

        Also, in case of a rain forecast, a 'porciento_lluvia' key will be
        added, with the precent of rain for the day.        
        """

        forecast = {"day": day}

        for time in range(day * 24 * 60, (day + 1) * 24 * 60):
            weather = self.solar_system.check_weather("minutes", time)
            if weather:
                if isinstance(weather, float):
                    forecast["weather"] = "rain"
                    forecast["amount"] = round(weather, 2)
                else:
                    forecast["weather"] = weather
                return forecast
        forecast["weather"] = "unstable"
        return forecast

    def forecast_period(self, start_day=0, end_day=3601):
        """Return  list of forecast dicts for the given period.

        The priod must be a two values indexable with the start and end days
        of the desired forecast period.
        """

        period_forecast = []
        for day in range(start_day, end_day):
            period_forecast.append(self.forecast_day(day))
        return period_forecast

    def forecast_iterator(self, start_day=0, end_day=3601):
        """Yield the forecast dict for each of the days in the given period.

        The priod must be a two values indexable with the start and end days
        of the desired forecast period.
        """

        for day in range(start_day, end_day):
            yield self.forecast_day(day)

    def emit_forecast_day(self, day):
        """Forecast and emit the given day's weather."""

        self.emitter.emit(self.forecast_day(day))
        self.emitter.close()

    def emit_forecast_period(self, start_day=0, end_day=3601):
        """Forecast and emit the given period's weather."""

        for day in range(start_day, end_day):
            self.emitter.emit(self.forecast_day(day))
            self.emitter.close()
