"""Geometry tools for Helios.

Most notably, here is Polygon, the core of weather prediction, whih has the
ability of calculating it's area, points aliniation, point pertenence,
perimeter, and more.
"""

from itertools import tee
from math import sqrt

import numpy as np


def pairwise(iterable):
    """Return an iterable of tuples of pairs of values of the original iterable

    Or, in other words: s -> (s0,s1), (s1,s2), (s2, s3), ...

    https://docs.python.org/3/library/itertools.html#itertools-recipes
    """

    first_value, second_value = tee(iterable)
    next(second_value, None)
    return zip(first_value, second_value)


class Polygon():
    """A figure composed of a series of points."""

    def __init__(self, x_values, y_values):

        self.x_values = x_values
        self.y_values = y_values

    def calculate_area(self):
        """Return the area of the Polygon."""

        x_dot_product = np.dot(self.x_values, np.roll(self.y_values, 1))
        y_dot_product = np.dot(self.y_values, np.roll(self.x_values, 1))
        return np.abs(x_dot_product - y_dot_product) / 2

    def closed_points(self):
        """Return the 'closed' points of the polygon.

        That means, adding the first point of the polygon to the list of points
        so you can add the last point to the first and 'close' the figure.
        """

        closed_x_values = self.x_values + [self.x_values[0]]
        closed_y_values = self.y_values + [self.y_values[0]]

        return closed_x_values, closed_y_values

    def closed_point_pairs(self):
        """Returns an iterator of pairs of the 'closed' points of the polygon.

        So that from ((x0, x1, x2), (y0, y1, y2)) you get:

        ((x0, x1), (y0, y1)) -> ((x1, x2), (y1, y2)) -> ((x2, x3), (y2, y3))

        See the description of 'closed_points' for info about what is 'closed'.
        """

        closed_x_values, closed_y_values = self.closed_points()
        return zip(pairwise(closed_x_values), pairwise(closed_y_values))

    def calculate_perimeter(self):
        """Return the perimeter of the Polygon."""

        perimeter = [0]
        for x_pair, y_pair in self.closed_point_pairs():
            x_difference = abs(y_pair[0] - y_pair[1])
            y_difference = abs(x_pair[0] - x_pair[1])

            perimeter.append(
                sqrt(pow(x_difference, 2) + pow(y_difference, 2))
            )
        return sum(perimeter)

    def check_points_alignment(self):
        """Return True if the polygon's points are aligned betwen them.

        The operational definition for this is that the polygon's area is of
        50km2 or less. Which seems reasonable for astronomical magnitudes.
        """

        area = round(self.calculate_area(), 9)
        if area < 50:
            return True
        return False

    def check_point_pertenence(self, point):
        """Check if the given point is inside the plygon area

        This is done by adding the area of many triangles created from the
        given point to each of 2 point pairs of the polygon (the first and
        second point with the given one, the second and the third, etc.),
        plus an extra one for the "closing" triangle from the last point with
        the first one.
        """

        partial_areas = []
        triangle_bases = self.closed_point_pairs()

        for triangle_base_x, triangle_base_y in triangle_bases:
            triangle = Polygon(
                triangle_base_x + (point[0],),
                triangle_base_y + (point[1],)
            )
            partial_areas.append(triangle.calculate_area())

        area_difference = round(sum(partial_areas) - self.calculate_area(), 9)
        return True if area_difference == 0 else False
