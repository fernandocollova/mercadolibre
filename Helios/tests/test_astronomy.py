from unittest import TestCase
from Helios.core.astronomy import SolarSystem, Polygon, Planet


class TestAstronomy(TestCase):

    def test_days_correlation(self):
        """The days with rain should not have small discontinuities"""

        planet1 = Planet(distance=500, speed=1)
        planet3 = Planet(distance=1000, speed=-5)
        planet2 = Planet(distance=2000, speed=3)
        system = SolarSystem(planets=[planet1, planet2, planet3])

        last_time = 0
        for time in range(1, 50000):
            polygon = Polygon(*system.make_polygon("minutes", time))
            if polygon.check_point_pertenence([0, 0]):
                self.assertNotEqual(time - last_time, 2)
                last_time = time

    def test_days_perimeter_top(self):
        """The perimeter of the triangles can't be bigger than 6305

        For this test, the biggest number we should get is 6179.58.
        However, the closest it gets is 6262.55 which is a 1.34% difference
        from that.

        Knowing there is what seems to be a rounding error, the test is rounded
        up to a 2% max difference.
        """
        planet1 = Planet(distance=500, speed=1)
        planet3 = Planet(distance=1000, speed=-5)
        planet2 = Planet(distance=2000, speed=3)
        system = SolarSystem(planets=[planet1, planet2, planet3])

        big_perimeters = []
        for time in range(1, 50000):
            polygon = Polygon(*system.make_polygon("minutes", time))
            perimeter = polygon.calculate_perimeter()
            if perimeter > 6305:
                big_perimeters.append(perimeter)
        self.assertEqual(big_perimeters, [])

    def test_there_are_aligned_points(self):
        """All planets must be aligned at some point."""

        planet1 = Planet(distance=500, speed=1)
        planet3 = Planet(distance=1000, speed=-5)
        planet2 = Planet(distance=2000, speed=3)
        system = SolarSystem(planets=[planet1, planet2, planet3])

        alignments = []
        for time in range(1, 50000):
            polygon = Polygon(*system.make_polygon("minutes", time))
            if polygon.check_points_alignment():
                alignments.append(time)
        self.assertNotEqual(alignments, [])
