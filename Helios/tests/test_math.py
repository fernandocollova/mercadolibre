from unittest import TestCase
from Helios.core.geometry import Polygon


class TestMath(TestCase):

    def test_area_works_with_triangle1(self):

        polygon = Polygon([-1, 0, 1], [-1, 1, -1])
        self.assertEqual(polygon.calculate_area(), 2)

    def test_area_works_with_triangle2(self):

        polygon = Polygon([-2, 0, 2], [-1, 3, -1])
        self.assertEqual(polygon.calculate_area(), 8)

    def test_area_works_with_square(self):
        polygon = Polygon([-1, -1, 1, 1], [-1, 1, 1, -1])
        self.assertEqual(polygon.calculate_area(), 4)

    def test_area_works_with_pentagon(self):

        polygon = Polygon([0, -10, -6, 6, 10, 0], [-10, -3, 8, 8, -3, -10])
        self.assertEqual(polygon.calculate_area(), 246)

    def test_true_point_pertenence(self):

        polygon = Polygon([-1, 0, 1], [-1, 1, -1])
        self.assertTrue(polygon.check_point_pertenence([0, 0]))

    def test_false_point_pertenence(self):

        polygon = Polygon([-1, 0, 1], [-1, 1, -1])
        self.assertFalse(polygon.check_point_pertenence([100, 100]))

    def test_inner_edge_point_pertenence(self):

        polygon = Polygon([-1, 0, 1], [-1, 1, -1])
        self.assertTrue(polygon.check_point_pertenence([-1, -1]))

    def test_outer_edge_point_pertenence(self):

        polygon = Polygon([-1, 0, 1], [-1, 1, -1])
        self.assertFalse(polygon.check_point_pertenence([0.6, 0]))
