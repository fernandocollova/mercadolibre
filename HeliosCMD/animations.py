"""Animation tools for Helios"""

import matplotlib.animation as animation
import matplotlib.pyplot as plt
from Helios.core.geometry import Polygon


class SolarSystemAnimation():
    """A matplotlib plot customized for showing SolarSystem objects."""

    def __init__(self):

        self.fig = plt.figure(figsize=(9, 9))
        self.axis = self.fig.add_subplot(1, 1, 1)
        self.axis.set_xlim([-2200, 2200])
        self.axis.set_ylim([-2200, 2200])
        self.axis.grid(color='b', linestyle='dotted', linewidth=0.5)
        self.axis.spines['left'].set_position('center')
        self.axis.spines['bottom'].set_position('center')
        self.axis.spines['right'].set_color('none')
        self.axis.spines['top'].set_color('none')
        self.axis.xaxis.set_ticks_position('bottom')
        self.axis.yaxis.set_ticks_position('left')
        self.axis.xaxis.set_ticklabels([])
        self.axis.yaxis.set_ticklabels([])
        self.axis.scatter(0, 0, s=2500, color="orange")
        self.fig.tight_layout()
        self.line, = plt.plot([], [], animated=True)
        self.default_speeds = {"days": 100, "hours": 10, "minutes": 10}

    def draw(self, i, time_unit, solar_system):
        """Draw the desired time."""

        polygon = Polygon(*solar_system.make_polygon(time_unit, i))
        self.line.set_data(*polygon.closed_points())
        return self.line,

    def draw_time(self, time, time_unit, solar_system):
        """Sow the drawing of a given time's position."""

        self.animate_time(time, time, time_unit, solar_system)

    def animate_time(self, start_time, end_time, time_unit, solar_system):
        """Animate the given time frame."""

        anim = animation.FuncAnimation(
            self.fig,
            self.draw,
            interval=self.default_speeds[time_unit],
            blit=True,
            fargs=(time_unit, solar_system),
            frames=range(start_time, end_time + 1),
        )
        plt.show(anim)
        plt.close()

    def animate(self, time_unit, solar_system):
        """Show an animation of the time passing"""

        anim = animation.FuncAnimation(
            self.fig,
            self.draw,
            interval=self.default_speeds[time_unit],
            blit=True,
            fargs=(time_unit, solar_system),
        )
        plt.show(anim)
        plt.close()
