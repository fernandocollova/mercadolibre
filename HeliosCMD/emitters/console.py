"""Collection of built-in emitters for console output."""


class Emitter():
    """Weather emitter that prints to stdout."""

    def __init__(self, clean=False):
        self.clean = clean

    def clean_and_emit(self, forecast):
        """Form a nice string from the forecast json."""

    def emit(self, forecast):
        """Emit the given forecast."""

        if self.clean:
            nice_string = "Day {} weather is expected to be {}"
            if forecast.get("amount"):
                nice_string += ", and the expected rain level will be {}"
            print(nice_string.format(*[value for value in forecast.values()]))
        else:
            print(forecast)

    def close(self):
        """There is nothing to close here."""
        pass
