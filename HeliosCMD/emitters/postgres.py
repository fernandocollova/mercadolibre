"""Collection of built-in emitters for db output."""

from os import getenv
from sqlalchemy import create_engine
from sqlalchemy import Column, String, Integer, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

base = declarative_base()

class Forecast(base):
    """A forecast stored in the DB"""

    __tablename__ = 'forecast'

    day = Column(Integer, primary_key=True)
    weather = Column(String)
    amount = Column(Float)


class Emitter():
    """Emitter for the SQLAlchemy ORM."""

    def __init__(self, conn_string=None):
        if not conn_string:
            conn_string = getenv("PSQL_CON_STRING")
            if conn_string is None:
                raise ValueError(
                    "You need a connection string for using this emitter"
                )
        self.engine = create_engine(conn_string)
        base.metadata.create_all(self.engine)
        session = sessionmaker(self.engine)
        self.session = session()

    def emit(self, forecast):
        """Emit the given forecast."""

        forecast = Forecast(**forecast)
        self.session.add(forecast)

    def close(self):
        """Commit and close the session with the DB."""

        self.session.commit()
        self.session.close()
