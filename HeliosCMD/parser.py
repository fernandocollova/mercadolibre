"""Command line options for Helio."""

import argparse


def argparser():
    """Conveninece function to parse"""

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--start-time',
        type=int,
        help="Start time for the animation",
        dest='start_time',
        required=False
    )

    parser.add_argument(
        '--end-time',
        type=int,
        help="End time for the animation",
        dest='end_time',
        required=False
    )

    parser.add_argument(
        '--time-unit',
        choices=['days', 'hours', 'minutes'],
        default='hours',
        help="Units in which the time is given",
        dest='time_unit',
        required=False
    )

    parser.add_argument(
        '--mode',
        choices=[
            'animate',
            'forecast',
            'summary',
        ],
        default='animate',
        help="Operation mode",
        dest='mode',
        required=True
    )

    parser.add_argument(
        '--animation-mode',
        choices=[
            'animate-period',
            'draw-time',
        ],
        default='animate',
        help="Animation operation mode",
        dest='animation_mode',
        required=False
    )

    parser.add_argument(
        '--forecast-mode',
        choices=[
            'forecast-period',
            'forecast-day',
        ],
        default='forecast',
        help="Animation operation mode",
        dest='forecast_mode',
        required=False
    )

    parser.add_argument(
        '--forecast-emitter',
        type=str,
        default='HeliosCMD.emitters.console',
        help="Forecast Emitter",
        dest='emitter',
        required=False
    )

    parser.add_argument(
        '--env-file',
        type=str,
        help="Path to load an env file",
        default=None,
        dest='env_path',
        required=False
    )

    return parser.parse_args()
