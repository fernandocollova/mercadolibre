#!python3

import importlib
from os import getenv
from Helios.core.astronomy import Planet, SolarSystem
from Helios.core.forecast import WeatherForecast
from HeliosCMD.animations import SolarSystemAnimation
from HeliosCMD.parser import argparser
from dotenv import load_dotenv

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from HeliosCMD.emitters.postgres import Forecast
from sqlalchemy.sql.expression import func


SOLAR_SYSTEM = SolarSystem(
    [
        Planet(distance=500, speed=1),
        Planet(distance=1000, speed=-5),
        Planet(distance=2000, speed=3)
    ]
)


def animation_demo(animation_mode, start_time, end_time, time_unit):
    """Animation demo of the defult SolarSystem."""

    SOLAR_SYSTEM.animation = SolarSystemAnimation()
    if animation_mode == 'animate':
        SOLAR_SYSTEM.animate(time_unit=time_unit)
    elif animation_mode == 'draw-time':
        SOLAR_SYSTEM.draw_time(time=start_time, time_unit=time_unit)
    elif animation_mode == 'animate-period':
        SOLAR_SYSTEM.animate_time(start_time, end_time, time_unit)


def forecast_demo(forecast_mode, emitter, start_time, end_time):
    """Animation demo of the defult SolarSystem."""

    emmiter_modeule = importlib.import_module(emitter)
    emmiter = emmiter_modeule.Emitter()
    forecast = WeatherForecast(SOLAR_SYSTEM, emmiter)
    if forecast_mode == 'forecast':
        print(forecast.forecast_period())
    elif forecast_mode == 'forecast-day':
        forecast.emit_forecast_day(start_time)
    elif forecast_mode == 'forecast-period':
        forecast.emit_forecast_period(start_time, end_time)


def forecast_summary():
    """
    ¿Cuántos períodos de sequía habrá?
    ¿Cuántos períodos de lluvia habrá y qué día será el pico máximo de lluvia?
    ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?
    """

    conn_string = getenv("PSQL_CON_STRING")
    if conn_string is None:
        raise ValueError(
            "You need to stup PSQL_CON_STRING for this operation."
        )
    engine = create_engine(conn_string)
    session_class = sessionmaker(engine)
    session = session_class()

    print(
        "\nThere will be {} days of drought in the next 10 years\n".format(
            session
            .query(func.count(Forecast.weather))
            .filter(Forecast.weather == "drought").first()[0]
        )
    )
    print(
        "There will be {} days of rain in the next 10 years\n".format(
            session
            .query(func.count(Forecast.weather))
            .filter(Forecast.weather == "rain").first()[0]
        )
    )
    print(
        "And in the most rainy day there will be {} rain\n".format(
            session.query(func.max(Forecast.amount)).first()[0]
        )
    )
    print(
        "There will be {} days of rain in the next 10 years\n".format(
            session
            .query(func.count(Forecast.weather))
            .filter(Forecast.weather == "optimal").first()[0]
        )
    )
    session.close()


def cmd():
    """Entrypoint when executing as a script."""

    args = argparser()

    if args.env_path:
        load_dotenv(args.env_path)

    if args.mode == "animate":
        animation_demo(
            args.animation_mode,
            args.start_time,
            args.end_time,
            args.time_unit
        )
    elif args.mode == "forecast":
        forecast_demo(
            args.forecast_mode,
            args.emitter,
            args.start_time,
            args.end_time,
        )
    elif args.mode == "summary":
        forecast_summary()
    else:
        raise ValueError("There was some horrible mistake with the parser.")


if __name__ == "__main__":
    cmd()
