# Helios forecaster

![Helios](https://1.bp.blogspot.com/-e_mpJmKB9k8/UhYWrz7PhkI/AAAAAAAAAJ0/DSrv_P6JjZ8/w1200-h630-p-k-no-nu/Helios.jpg)

## An app to forecast the weather

The objective of this program is to be a library to forecast the weather of planets orbiting around the 40 Eridani A star.

And also, for all our Vulcan, Ferengi and Betazoid users, this is also an app for forecasting the weather in your specific system. With some bonus visualiztion suff on it.

## How to use it?

First of all, you need to install it with `pip3 install --user git+https://fernandocollova@bitbucket.org/fernandocollova/mercadolibre.git`

Then, if you want today's forecst for Ferenginar, Vulcan and Betazoid, you can run:

`weather.py --mode forecast --forecast-mode "forecast-day" --start-time=0 --forecast-emitter "HeliosCMD.emitters.console"`

Or, if you want it for the next 10 days:

`weather.py --mode forecast --forecast-mode "forecast-period" --start-time=1 --end-time=11 --forecast-emitter "HeliosCMD.emitters.console"`

Or maybe you want to work with the generated data. Then, you can log the output to a PostgreSQL database:

- First create a .env file wherever you want, with `PSQL_CON_STRING=[the_actual_connection_string]`

- The user in the connection string must have CREATE privileges on the DB, to create the `forecast` table, or you must create it first as described in HeliosCMD.emitters.postgres.Forecast

Then run:

`weather.py --mode forecast --forecast-mode "forecast-period" --start-time=1 --end-time=360 --forecast-emitter "HeliosCMD.emitters.postgres" --env-file=".env"`

And, that's mosttly it when it comes to forecast from the command line. There are more options to choose from, but they are pretty self explainatory, so check them with `weather.py --help`

## That's too much work

Yeah, probably, that's why you don't really have to do most of that it you just want the weather as you can use [this awesome api](https://q92t4vns87.execute-api.us-west-2.amazonaws.com/prod/Helios?dia=18) that you can you by passing any day as a parameter.

## And what about the 'libary' part of this?

Well, glad you ask handsom stranger. This app provides some classes that might be useful for your needs. They are mostly docuemented, but an overview would be as follows:

- All of the base building blocks are in core
- core.forecast.Polygon will give you operations over polygons of an arbitrary number of sides, like calculating area, perimeter, points alineation, or point pertenence.
- core.astronomy.Planet describes a planet and can get you it's  position around a star
- core.astronomy.SolarSystem can do the same but for an arbitary nunmber of planets, but also check for a given time's weather, and animate the movement of the planets if given an anmination object. All that with variable percission.
- core.forecast.WeatherForecast can do more "useful" weather forecasting, like forecasting the weather of a full given day, and not of only a given minute. Also, it can emit it's forecasts to an emitter.

## Emitters? Animaton Objects? What's that?

That's the way that Helios uses to communicate with the world. Mostly because [Hermes](https://bitbucket.org/fernandocollova/hermes/overview) is ill.

The idea is that if you implement something that can `.emit()` and `.close()` you can pass it to a WeatherForecast and it will send its forecast to it when you generate them.

You can either implement yourself your code, or pass it as a full python path with the command line option `--forecast-emiter` and the program will load it.

The animations also work mostly the same way, but they are more complicated, so check the docs on them.

## Animations? So I can see pretty graphics with this?

Well, I don't know about pretty, but you can try:

`weather.py --mode animate --animation-mode "animate-period" --start-time=25920 --end-time=27360 --time-unit="minutes"`

And see the very moment when Ferenginar, Vulcan and Betazoid allign themselves but not with the sun

Or you can run:

`weather.py --mode "animate" --animation-mode "animate-period" --start-time=25920 --end-time=27360 --time-unit="hours"`

To see the planets go from drought to unstable weather, to optimal conditions, to weather, and to unstable conditions again.

## Anything else?

You can also have a summary of the conditions for the next ten yeras with:

`weather.py --mode summary --env-file=".env"`

Please note that level of summary requires you to have previously generated those 10 years of data. Fortunately, you can do so with this simple command. And a hell lot of patinece :/

`weather.py --mode forecast --forecast-mode "forecast-period" --start-time=0 --end-time=3600 --forecast-emitter "HeliosCMD.emitters.postgres" --env-file=".env"`
