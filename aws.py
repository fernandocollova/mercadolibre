from json import dumps
from Helios.core.astronomy import Planet, SolarSystem
from Helios.core.forecast import WeatherForecast


def lambda_handler(event, context):
    """Entrypoint for AWS lambda."""

    planet1 = Planet(distance=500, speed=1)
    planet3 = Planet(distance=1000, speed=-5)
    planet2 = Planet(distance=2000, speed=3)
    system = SolarSystem([planet1, planet2, planet3])
    forecast = WeatherForecast(system)

    return {
        "statusCode": "200",
        "headers": {},
        "body": dumps(
            forecast.forecast_day(int(event["queryStringParameters"]["dia"]))
        )
    }
