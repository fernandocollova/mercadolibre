#!/bin/bash

set -e

# Test the code
if [ ! -e ./venv ] ; then
    virtualenv venv --python=python3
    source venv/bin/activate
    pip3 -q install --upgrade pip
    pip3 install --upgrade --compile ./
    python3 -m unittest discover -v "$1.tests"
fi

# Create the build env
if [ -e ./.build/ ]; then rm -r ./.build/; fi
mkdir ./.build/
pip3 install --upgrade --compile --ignore-installed ./ -t ./.build
cp ./aws.py ./.build/aws.py

# Zip the package
if [ -e ./$1.zip ]; then rm ./$1.zip; fi
cd ./.build/
zip -9 -r ../$1 ./
cd ..

# Upload to AWS
aws --profile lambda lambda update-function-code --function-name $1 --zip-file fileb://./$1.zip

# Clean after you leave
rm -r .build
