from setuptools import setup

setup(
    name='Helios',
    packages=[
        'Helios',
        'Helios.core',
        'Helios.tests',
        'HeliosCMD',
        'HeliosCMD.emitters',
    ],
    version="1.0.2",
    description='Helios weather forecaster',
    scripts=['HeliosCMD/weather.py'],
    install_requires=[
        "matplotlib",
        "numpy",
        "argparse",
        "python-dotenv",
        "sqlalchemy",
    ]
)
